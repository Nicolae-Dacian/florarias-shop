package org.example.entity;

public enum ProductType {
    NATURAL_FLOWERS, ARTIFICIAL_FLOWER,GIFT_CARD;
}
