package org.example.entity;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Product {

    @Id
    private Integer id;
    @Enumerated(value = EnumType.STRING)
    @Column(name = "product_type")
    private ProductType productType;
    @Column(nullable = false)
    private String name;
    private String description;
    private Integer quantity;
    @Column(name = "buying_prices")
    private Double buyingPrice;
    @Column(name = "selling_prices")
    private Double sellingPrice;
    @Column(name = "buying_date")
    private LocalDate buyingDate;
    @Column(name = "expiration_Date")
    private LocalDate expirationDate;


}
