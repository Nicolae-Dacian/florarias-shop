package org.example.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "user_feedback")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

public class UserFeedback {

    @Id
    private Integer id;

    @Enumerated(value = EnumType.STRING)
    private Rating rating;

    @Column(name = "feedback_description")
    private String feedbackDescription;

    public String string(){
        String s = "User feetback"+ id+ "este"+ rating+ ":" + feedbackDescription;
        return s;
    }

}