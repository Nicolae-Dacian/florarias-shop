package org.example;

import org.example.config.DatabaseConfig;
import org.example.entity.Rating;
import org.example.entity.UserFeedback;
import org.example.repository.UserFeedbackRepository;

public class Main2 {
    private static UserFeedbackRepository userFeedbackRepo = new UserFeedbackRepository(DatabaseConfig.
            getSessionFactory());

    public static void main(String[] args) {
        UserFeedback uf1 = new UserFeedback(1, Rating.GOOD, "Super florarie");
        UserFeedback uf2 = new UserFeedback(2, Rating.GOOD, "Super florarie");

        userFeedbackRepo.createUserFeedback(uf1);
        userFeedbackRepo.createUserFeedback(uf2);

        System.out.println(userFeedbackRepo.getAllUserFeedbacks());

        uf1.setFeedbackDescription("Ne-a placut foarte mult floraria");

        userFeedbackRepo.updateUserFeedback(uf1);
        System.out.println(userFeedbackRepo.getAllUserFeedbacks());

        userFeedbackRepo.deleteUserFeedback(uf2);
        System.out.println(userFeedbackRepo.getAllUserFeedbacks());

    }
}