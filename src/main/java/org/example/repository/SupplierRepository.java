package org.example.repository;

import org.example.entity.Supplier;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class SupplierRepository {

    private final SessionFactory sessionFactory;
    public SupplierRepository(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }
    //salveaza un supplier in baza de date
    public  void save(Supplier supplier){
        Session session = sessionFactory.openSession();//deschide o sesiune de comunicare cu baza de date
        Transaction transaction = session.beginTransaction();//incepe o tranzactie

        session.persist(supplier);// avem o singura modificare de facut si anume:salveaza supplier-ull

        transaction.commit();//salveaza in baza de date modificarile facute dupa deschiderea trenzactiei
        session.close();//inchide sesiune de comunicare cu baza de date
    }
}
