package org.example.repository;


import org.example.config.DatabaseConfig;
import org.example.entity.UserFeedback;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class UserFeedbackRepository {

    private final SessionFactory sessionFactory = DatabaseConfig.getSessionFactory();

    public UserFeedbackRepository(SessionFactory sessionFactory) {
    }

    public void createUserFeedback(UserFeedback uf) {
        Session s = sessionFactory.openSession();
        Transaction t = s.beginTransaction();
        s.persist(uf);
        t.commit();
        s.close();
    }

    public List<UserFeedback> getAllUserFeedbacks() {
        Session s = sessionFactory.openSession();
        List<UserFeedback> userFeedbackList = s.createQuery("SELECT u FROM UserFeedback u", UserFeedback.class)
                .getResultList();
        s.close();
        return userFeedbackList;
    }

    public void updateUserFeedback(UserFeedback uf) {
        Session s = sessionFactory.openSession();
        Transaction t = s.beginTransaction();
        s.merge(uf);
        t.commit();
        s.close();
    }
    public void deleteUserFeedback(UserFeedback uf){
        Session s = sessionFactory.openSession();
        Transaction t = s.beginTransaction();
        s.remove(uf);
        t.commit();
        s.close();
    }


}
