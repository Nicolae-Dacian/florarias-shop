package org.example.repository;

import org.example.entity.Product;
import org.example.entity.Supplier;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class ProductRepository {

    private final SessionFactory sessionFactory;
    public ProductRepository(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }
    //salveaza un supplier in baza de date
    public  void save( Product product){
        Session session = sessionFactory.openSession();//deschide o sesiune de comunicare cu baza de date
        Transaction transaction = session.beginTransaction();//incepe o tranzactie

        session.persist(product);// avem o singura modificare de facut si anume:salveaza supplier-ull

        transaction.commit();//salveaza in baza de date modificarile facute dupa deschiderea trenzactiei
        session.close();//inchide sesiune de comunicare cu baza de date
    }
    public List<Product>getAll(){
        List<Product>products;
        Session session=sessionFactory.openSession();
        products=session.createQuery("SELECT p From Product p", Product.class).getResultList();
        session.close();
        return products;
    }
}
