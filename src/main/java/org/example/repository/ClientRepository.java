package org.example.repository;

import org.example.entity.Client;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class ClientRepository {
    private final SessionFactory sessionFactory;

    public ClientRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    //salveaza un client in baza de date
    public void save(Client client) {
        Session session = sessionFactory.openSession();//deschide o sesiune de comunicare cu baza de date
        Transaction transaction = session.beginTransaction();//incepe o tranzactie

        session.persist(client);// avem o singura modificare de facut si anume:salveaza supplier-ull

        transaction.commit();//salveaza in baza de date modificarile facute dupa deschiderea trenzactiei
        session.close();//inchide sesiune de comunicare cu baza de date
    }

    public List<Client> getAll() {
        List<Client> clients ;
        Session session = sessionFactory.openSession();
        //sql: SELECT CLIENT;
        //acesta este un query de hibernate(e usor diferit de unquery-ul din SQL
        //HQL: Select c From From Client c
        //selecteaza toate randurile din tabela Client
        //"c" este doar un alias pentru un rand
        clients = session.createQuery("SELECT c FROM Client c", Client.class).getResultList();
        session.close();
        return clients;
    }
}
