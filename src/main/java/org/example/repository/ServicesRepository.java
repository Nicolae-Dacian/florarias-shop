package org.example.repository;

import org.example.entity.Services;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import java.util.List;

public class ServicesRepository {

    private final SessionFactory sessionFactory;

    public ServicesRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    public void save(Services services) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        session.persist(services);

        transaction.commit();
        session.close();
    }

    public  List<Services> getAll() {

        List<Services> services ;

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        services = session.createQuery("SELECT s FROM Services s", Services.class).getResultList();
        session.close();
        return services;
    }
}
