package org.example;


import org.example.config.DatabaseConfig;
import org.example.entity.Client;
import org.example.entity.Product;
import org.example.entity.ProductType;
import org.example.entity.Supplier;
import org.example.repository.ClientRepository;
import org.example.repository.ProductRepository;
import org.example.repository.SupplierRepository;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import java.time.LocalDate;
import java.util.List;
public class Main {
    public static SupplierRepository supplierRepository = new SupplierRepository(DatabaseConfig.getSessionFactory());
    public static ClientRepository clientRepository = new ClientRepository(DatabaseConfig.getSessionFactory());
    public static ProductRepository productRepository = new ProductRepository(DatabaseConfig.getSessionFactory());
    public static void main(String[] args) {
        Supplier s1 = new Supplier(1, "Pepeniera Marioara", "Brasov - Romania");
        Supplier s2 = new Supplier(2, "Depozitul de lalele", "Olanda");
        supplierRepository.save(s1);
        supplierRepository.save(s2);
        Client c1 = new Client(1, "Razvan", "George", "razvan_george95@yahoo.com");
        Client c2 = new Client(2, "Diana", "Maria", "diana_maria@yahoo.com");
        clientRepository.save(c1);
        clientRepository.save(c2);
        Product p1 = new Product(1, ProductType.NATURAL_FLOWERS,
                "Trandafiri", "Rosii", 11, 280.00, 300.00,
                LocalDate.of(2023,6, 1),
                LocalDate.of(2023, 6, 10));
        Product p2 = new Product(2, ProductType.NATURAL_FLOWERS,
                "Trandafiri", "Galbeni", 18, 350.00, 400.00,
                LocalDate.of(2023,7, 1),
                LocalDate.of(2023, 7, 10));
        productRepository.save(p1);
        productRepository.save(p2);
        displayAllClient();
        displayAllProduct();
    }
    public static void displayAllClient() {
        List<Client> clients = clientRepository.getAll();
        for(Client c : clients) {
            System.out.println("Nume: " + c.getLastName() + " Prenume: " + c.getFirstName() + " Email: " + c.getEmail());
        }
    }
    public static void displayAllProduct() {
        List<Product> products = productRepository.getAll();
        for(Product p : products) {
            System.out.println(p.getName() + " " + p.getDescription() + " " + p.getQuantity() +
                    " bucati" + "," + " cumparati la data de: " + p.getBuyingDate()+ "," +
                    " cu pretul de: " + p.getBuyingPrice() + "," + " vanduti cu pretul de: " + p.getSellingPrice() +
                    "," + " expira la data de: " + p.getExpirationDate() );
        }
    }
}







